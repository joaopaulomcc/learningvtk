import vtk
import vtk.util.colors as colors
import numpy as np


def add_source(source, renderer, color=colors.tomato, flat_shading=True):

    source_mapper = vtk.vtkPolyDataMapper()
    source_mapper.SetInputConnection(source.GetOutputPort())
    source_mapper.ScalarVisibilityOff()
    source_actor = vtk.vtkActor()
    source_actor.SetMapper(source_mapper)

    source_actor.GetProperty().SetColor(color)

    if flat_shading:
        source_actor.GetProperty().SetInterpolationToFlat()

    renderer.AddActor(source_actor)

    return source_actor


def add_axis(renderer, scale):

    # Arrow
    arrow = vtk.vtkArrowSource()
    arrow.SetTipLength(0.05)
    arrow.SetTipRadius(0.1)
    arrow.SetTipResolution(25)
    arrow.SetShaftRadius(0.03)
    arrow.SetShaftResolution(25)
    arrow.SetInvert(False)

    x_axis = add_source(arrow, renderer, colors.red, flat_shading=True)
    y_axis = add_source(arrow, renderer, colors.green, flat_shading=True)
    z_axis = add_source(arrow, renderer, colors.blue, flat_shading=True)
    x_axis.SetScale([scale, 0.5, 0.5])
    y_axis.SetScale([scale, 0.5, 0.5])
    y_axis.RotateZ(90)
    z_axis.SetScale([scale, 0.5, 0.5])
    z_axis.RotateY(-90)


renderer = vtk.vtkRenderer()

add_axis(renderer, 10)

# ======================================================================================
# Cone
cone_source = vtk.vtkConeSource()
cone_source.SetHeight(5)
cone_source.SetRadius(2)
cone_source.SetResolution(25)
cone_source.SetCenter([0, 0, 0])
# cone_source.SetDirection([0, 0, 1])

cone_actor = add_source(cone_source, renderer)
# cone_actor2 = add_source(cone_source, renderer)

# cone_actor.SetPosition(np.array([5, 5, 5]))
# cone_actor.AddPosition(np.array([0, 5, 0]))
# cone_actor.RotateY(90)
# cone_actor.RotateX(90)
# cone_actor.RotateZ(90)
# cone_actor.SetOrientation(np.array([90, 90, 0]))
# cone_actor.AddOrientation(np.array([90, 90, 0]))
# cone_actor.RotateWXYZ(45, 1, 1, 1)
# cone_actor.SetScale(np.array([5, 5, 5]))
# cone_actor.SetOrigin(np.array([0, 5, 0]))
cone_actor.GetProperty().SetOpacity(0.5)

# Cube
cube_source = vtk.vtkCubeSource()
cube_source.SetXLength(2)
cube_source.SetYLength(2)
cube_source.SetZLength(2)
cube_source.SetCenter(0, 0, 0)
cube_actor = add_source(cube_source, renderer)

cube_actor.AddPosition(np.array([5, 5, 0]))
cube_actor.GetProperty().SetRepresentationToWireframe()

# Cylinder
cylinder_source = vtk.vtkCylinderSource()
cylinder_source.SetHeight(5)
cylinder_source.SetRadius(2)
cylinder_source.SetCenter([0, 0, 0])
cylinder_source.SetResolution(15)
cylinder_source.SetCapping(True)
cylinder_actor = add_source(cylinder_source, renderer)

cylinder_actor.AddPosition(np.array([10, 10, 0]))


property = vtk.vtkProperty()
property.SetOpacity(0.25)
property.SetAmbient(0.5)
property.SetDiffuse(0.6)
property.SetSpecular(1.0)
property.SetSpecularPower(10.0)
property.SetColor(0.1, 0.2, 0.5)
property.SetAmbientColor(0.1, 0.1, 0.1)
property.SetDiffuseColor(0.1, 0.2, 0.4)
property.SetSpecularColor(1, 1, 1)
property.SetOpacity(1)
# property.SetRepresentationToWireframe()
cylinder_actor.SetProperty(property)


# Assembly
assembly = vtk.vtkAssembly()
assembly.AddPart(cylinder_actor)
assembly.AddPart(cube_actor)
assembly.SetOrigin(5, 5, 5)
assembly.RotateX(45)

renderer.AddActor(assembly)

# ======================================================================================
window = vtk.vtkRenderWindow()
window.AddRenderer(renderer)
window.SetDesiredUpdateRate(5.0)

interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(window)
interactor_style = vtk.vtkInteractorStyleTrackballCamera()
interactor.SetInteractorStyle(interactor_style)

renderer.SetBackground(1, 1, 1)
window.SetSize(800, 800)

interactor.Initialize()

renderer.ResetCamera()
renderer.GetActiveCamera().Zoom(1)
window.Render()

interactor.Start()
