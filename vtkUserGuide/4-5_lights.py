import vtk

cadpart = vtk.vtkSTLReader()
cadpart.SetFileName("../vtk-data/Data/42400-IDGH.stl")

cadpart_mapper = vtk.vtkPolyDataMapper()
cadpart_mapper.SetInputConnection(cadpart.GetOutputPort())

cadpart_actor = vtk.vtkLODActor()
cadpart_actor.SetMapper(cadpart_mapper)

ren1 = vtk.vtkRenderer()
ren_win = vtk.vtkRenderWindow()
ren_win.AddRenderer(ren1)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(ren_win)
style = vtk.vtkInteractorStyleTrackballCamera()
iren.SetInteractorStyle(style)

ren1.AddActor(cadpart_actor)
ren1.SetBackground(0.1, 0.2, 0.4)
ren_win.SetSize(500, 500)

cam1 = ren1.GetActiveCamera()

red_light = vtk.vtkLight()
red_light.SetColor(1, 0, 0)
red_light.SetFocalPoint(cam1.GetFocalPoint())
red_light.SetPosition(cam1.GetPosition())
red_light.PositionalOn()
red_light.SetConeAngle(90)

ren1.AddLight(red_light)


iren.Initialize()

ren1.ResetCamera()
ren_win.Render()

iren.Start()
