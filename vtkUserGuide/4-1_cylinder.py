import vtk
import vtk.util.colors as colors


cylinder = vtk.vtkCylinderSource()
cylinder.SetResolution(100)

cylinder_mapper = vtk.vtkPolyDataMapper()
cylinder_mapper.SetInputConnection(cylinder.GetOutputPort())

cylinder_actor = vtk.vtkActor()
cylinder_actor.SetMapper(cylinder_mapper)
cylinder_actor.GetProperty().SetColor(colors.tomato)
cylinder_actor.RotateX(30)
cylinder_actor.RotateY(-45)

ren1 = vtk.vtkRenderer()
ren_win = vtk.vtkRenderWindow()
ren_win.AddRenderer(ren1)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(ren_win)
style = vtk.vtkInteractorStyleTrackballCamera()
iren.SetInteractorStyle(style)

ren1.AddActor(cylinder_actor)
ren1.SetBackground(0.1, 0.2, 0.4)
ren_win.SetSize(500, 500)

iren.Initialize()

ren1.ResetCamera()
ren1.GetActiveCamera().Zoom(1.5)
ren_win.Render()

iren.Start()
