import vtk
import vtk.util.colors as colors


cadpart = vtk.vtkSTLReader()
cadpart.SetFileName("../vtk-data/Data/42400-IDGH.stl")

shrink = vtk.vtkShrinkPolyData()
shrink.SetInputConnection(cadpart.GetOutputPort())
shrink.SetShrinkFactor(0.85)

cadpart_mapper = vtk.vtkPolyDataMapper()
cadpart_mapper.SetInputConnection(shrink.GetOutputPort())

cadpart_actor = vtk.vtkLODActor()
cadpart_actor.SetMapper(cadpart_mapper)

#cadpart_actor.GetProperty().SetColor(colors.tomato)
#cadpart_actor.RotateX(30)
#cadpart_actor.RotateY(-45)

ren1 = vtk.vtkRenderer()
ren_win = vtk.vtkRenderWindow()
ren_win.AddRenderer(ren1)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(ren_win)
style = vtk.vtkInteractorStyleTrackballCamera()
iren.SetInteractorStyle(style)

ren1.AddActor(cadpart_actor)
ren1.SetBackground(0.1, 0.2, 0.4)
ren_win.SetSize(500, 500)

iren.Initialize()

ren1.ResetCamera()
ren1.GetActiveCamera().Zoom(1.5)
ren_win.Render()

iren.Start()
