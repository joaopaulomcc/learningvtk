import vtk

cadpart = vtk.vtkSTLReader()
cadpart.SetFileName("../vtk-data/Data/42400-IDGH.stl")

cadpart_mapper = vtk.vtkPolyDataMapper()
cadpart_mapper.SetInputConnection(cadpart.GetOutputPort())

cadpart_actor = vtk.vtkLODActor()
cadpart_actor.SetMapper(cadpart_mapper)

ren1 = vtk.vtkRenderer()
ren_win = vtk.vtkRenderWindow()
ren_win.AddRenderer(ren1)
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(ren_win)
style = vtk.vtkInteractorStyleTrackballCamera()
iren.SetInteractorStyle(style)

ren1.AddActor(cadpart_actor)
ren1.SetBackground(0.1, 0.2, 0.4)
ren_win.SetSize(500, 500)

iren.Initialize()

ren1.ResetCamera()
cam = ren1.GetActiveCamera()
cam.ParallelProjectionOn()

cam1 = vtk.vtkCamera()
cam1.SetFocalPoint(0, 0, 0)
cam1.SetPosition(10, 10, 10)
cam1.ComputeViewPlaneNormal()
cam1.SetViewUp(1, 0, 0)
cam1.OrthogonalizeViewUp()

ren1.SetActiveCamera(cam)

ren_win.Render()

iren.Start()
