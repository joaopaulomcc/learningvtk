import vtk

renderer = vtk.vtkRenderer()

# ======================================================================================

bmp_reader = vtk.vtkBMPReader()
bmp_reader.SetFileName("../data/masonry.bmp")

texture = vtk.vtkTexture()
texture.SetInputConnection(bmp_reader.GetOutputPort())
texture.InterpolateOn()

plane = vtk.vtkPlaneSource()
plane_mapper = vtk.vtkPolyDataMapper()
plane_mapper.SetInputConnection(plane.GetOutputPort())
plane_actor = vtk.vtkActor()
plane_actor.SetMapper(plane_mapper)
plane_actor.SetTexture(texture)

renderer.AddActor(plane_actor)

# ======================================================================================

window = vtk.vtkRenderWindow()
window.AddRenderer(renderer)
window.SetDesiredUpdateRate(5.0)

interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(window)
interactor_style = vtk.vtkInteractorStyleTrackballCamera()
interactor.SetInteractorStyle(interactor_style)

renderer.SetBackground(1, 1, 1)
window.SetSize(800, 800)

interactor.Initialize()

renderer.ResetCamera()
renderer.GetActiveCamera().Zoom(1)
window.Render()

interactor.Start()
