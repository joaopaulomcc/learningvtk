import numpy as np
import tools as t

import vtk


def test_plot_3d_lines():

    n_points = 10000

    theta = np.linspace(0, 10 * np.pi, n_points)
    z = np.linspace(0, np.pi, n_points)
    x0 = np.cos(theta) * np.sin(z)
    y0 = np.sin(theta) * np.sin(z)

    x1 = np.cos(theta + 2 * np.pi / 3) * np.sin(z)
    y1 = np.sin(theta + 2 * np.pi / 3) * np.sin(z)

    x2 = np.cos(theta + 4 * np.pi / 3) * np.sin(z)
    y2 = np.sin(theta + 4 * np.pi / 3) * np.sin(z)

    colors = [
        [1.0, 0.0, 0.0],
        [0.0, 1.0, 0.0],
        [0.0, 0.0, 1.0],
    ]

    renderer, interactor, window, actors = t.plot_3d_lines(
        [x0, x1, x2], [y0, y1, y2], [z, z, z], colors=colors, line_width=5
    )

    interactor.Start()


def test_plot_3d_points():

    n_points = 100

    theta = np.linspace(0, 10 * np.pi, n_points)
    z = np.linspace(0, np.pi, n_points)
    x0 = np.cos(theta) * np.sin(z)
    y0 = np.sin(theta) * np.sin(z)

    x1 = np.cos(theta + 2 * np.pi / 3) * np.sin(z)
    y1 = np.sin(theta + 2 * np.pi / 3) * np.sin(z)

    x2 = np.cos(theta + 4 * np.pi / 3) * np.sin(z)
    y2 = np.sin(theta + 4 * np.pi / 3) * np.sin(z)

    colors = [
        [1.0, 0.0, 0.0],
        [0.0, 1.0, 0.0],
        [0.0, 0.0, 1.0],
    ]

    renderer, interactor, window, actors = t.plot_3d_points(
        [x0, x1, x2], [y0, y1, y2], [z, z, z], colors=colors, point_size=5
    )

    interactor.Start()


def test_plot_3d_lines_color_data():

    n_points = 1000

    theta = np.linspace(0, 10 * np.pi, n_points)
    z = np.linspace(0, np.pi, n_points)
    x0 = np.cos(theta) * np.sin(z)
    y0 = np.sin(theta) * np.sin(z)

    x1 = np.cos(theta + 2 * np.pi / 3) * np.sin(z)
    y1 = np.sin(theta + 2 * np.pi / 3) * np.sin(z)

    x2 = np.cos(theta + 4 * np.pi / 3) * np.sin(z)
    y2 = np.sin(theta + 4 * np.pi / 3) * np.sin(z)

    renderer, interactor, window = t.plot_3d_lines(
        x_vectors=[x0, x1, x2],
        y_vectors=[y0, y1, y2],
        z_vectors=[z, z, z],
        data_vectors=[z, z, z],
        color_map="coolwarm",
        use_data_for_color=True,
        line_width=3.0,
    )

    t.save_picture(window, "lines.png")


def test_plot_3d_points_color_data():

    n_points = 100

    theta = np.linspace(0, 10 * np.pi, n_points)
    z = np.linspace(0, np.pi, n_points)
    x0 = np.cos(theta) * np.sin(z)
    y0 = np.sin(theta) * np.sin(z)

    x1 = np.cos(theta + 2 * np.pi / 3) * np.sin(z)
    y1 = np.sin(theta + 2 * np.pi / 3) * np.sin(z)

    x2 = np.cos(theta + 4 * np.pi / 3) * np.sin(z)
    y2 = np.sin(theta + 4 * np.pi / 3) * np.sin(z)

    renderer, interactor, window, actors = t.plot_3d_points(
        x_vectors=[x0, x1, x2],
        y_vectors=[y0, y1, y2],
        z_vectors=[z, z, z],
        data_vectors=[z, z, z],
        color_map="viridis",
        use_data_for_color=True,
        point_size=3.0,
    )

    interactor.Start()


def test_plot_3d_surfaces():

    resolution = 500

    x = np.linspace(-2 * np.pi, 2 * np.pi, resolution)
    y = np.linspace(-2 * np.pi, 2 * np.pi, resolution)
    xx, yy = np.meshgrid(x, y)

    zz = 2 * np.sin(0.2 * (xx ** 2 + yy ** 2))

    renderer, interactor, window, actors = t.plot_3d_surfaces(
        xx_matrices=xx,
        yy_matrices=yy,
        zz_matrices=zz,
        data_matrices=None,
        color_map="viridis",
        use_data_for_color=False,
        colors=[0.2, 0.75, 0.33],
    )

    interactor.Start()


def test_plot_3d_surfaces_color_data():

    resolution = 500

    x = np.linspace(-2 * np.pi, 2 * np.pi, resolution)
    y = np.linspace(-2 * np.pi, 2 * np.pi, resolution)
    xx, yy = np.meshgrid(x, y)

    zz = 2 * np.sin(0.2 * (xx ** 2 + yy ** 2))

    renderer, interactor, window, actors = t.plot_3d_surfaces(
        xx_matrices=xx,
        yy_matrices=yy,
        zz_matrices=zz,
        data_matrices=zz,
        color_map="viridis",
        use_data_for_color=True,
        colors=[1.0, 1.0, 1.0],
    )

    interactor.Start()


def test_text_2d_plot():

    text_actor = t.create_vtk_2d_text(
        text_string="Hello\nWorld!",
        position=[0.5, 0.5],
        font_size=35,
        text_color=[1.0, 1.0, 1.0],
        font_family="times",
        bold=True,
        italic=True,
        shadow=True,
        line_spacing=1.5,
        justification="center",
        vertical_justification="middle",
    )

    renderer, interactor, window, actors = t.create_vtk_scene(actors_2d=[text_actor])

    interactor.Start()


def test_text_3d_plot():

    text_actor0 = t.create_vtk_3d_text(
        text_string="Hello\nWorld!",
        position=[0, 0, 0],
        font_size=1,
        text_color=[1.0, 0.0, 0.0],
    )

    text_actor1 = t.create_vtk_3d_text(
        text_string="Hello\nWorld!",
        position=[2, 2, 2],
        font_size=1,
        text_color=[0.0, 1.0, 0.0],
    )

    text_actor2 = t.create_vtk_3d_text(
        text_string="Hello\nWorld!",
        position=[3, 3, 3],
        font_size=1,
        text_color=[0.0, 0.0, 1.0],
    )

    renderer, interactor, window = t.create_vtk_scene(
        followers=[text_actor0, text_actor1, text_actor2],
    )

    interactor.Start()


def test_create_animation():

    n_points = 10000

    theta = np.linspace(0, 10 * np.pi, n_points)
    z = np.linspace(0, np.pi, n_points)
    x0 = np.cos(theta) * np.sin(z)
    y0 = np.sin(theta) * np.sin(z)

    x1 = np.cos(theta + 2 * np.pi / 3) * np.sin(z)
    y1 = np.sin(theta + 2 * np.pi / 3) * np.sin(z)

    x2 = np.cos(theta + 4 * np.pi / 3) * np.sin(z)
    y2 = np.sin(theta + 4 * np.pi / 3) * np.sin(z)

    colors = [
        [1.0, 0.0, 0.0],
        [0.0, 1.0, 0.0],
        [0.0, 0.0, 1.0],
    ]

    renderer, interactor, window, actors = t.plot_3d_lines(
        [x0, x1, x2], [y0, y1, y2], [z, z, z], colors=colors, line_width=5
    )

    c_theta = np.linspace(0, 2 * np.pi, 61)
    c_x = 8 * np.cos(c_theta)
    c_y = 8 * np.sin(c_theta)
    c_z = 5 * np.full(len(c_y), 1.0)

    position = np.array([c_x, c_y, c_z]).transpose()
    camera_data = {
        "position": position,
        "focal_point": np.array([(0.0, 0.0, 0.5 * np.pi)] * len(c_x)),
        "view_up": np.array([(0.0, 0.0, 1.0)] * len(c_x)),
    }

    renderer, interactor, window = t.create_animation(
        actors=actors,
        actors_data=[],
        actors_2d=[],
        actors_2d_data=[],
        followers=[],
        followers_data=[],
        camera_data=camera_data,
        window_size=[1000, 1000],
        window_title="Plot",
        window_background=[0.0, 0.0, 0.0],
        refresh_rate=60,
        start_frame=0,
        n_frames=len(c_x),
        loop=False,
        save_frames_folder=None,
    )

    interactor.Start()


def test_create_animation_modify_points():

    n_points = 1000

    theta = np.linspace(0, 10 * np.pi, n_points)
    z = np.linspace(0, np.pi, n_points)
    x0 = np.cos(theta) * np.sin(z)
    y0 = np.sin(theta) * np.sin(z)

    x1 = np.cos(theta + 2 * np.pi / 3) * np.sin(z)
    y1 = np.sin(theta + 2 * np.pi / 3) * np.sin(z)

    x2 = np.cos(theta + 4 * np.pi / 3) * np.sin(z)
    y2 = np.sin(theta + 4 * np.pi / 3) * np.sin(z)

    renderer, interactor, window, actors = t.plot_3d_lines(
        x_vectors=[x0, x1, x2],
        y_vectors=[y0, y1, y2],
        z_vectors=[z, z, z],
        data_vectors=[z, z, z],
        color_map="rainbow",
        use_data_for_color=True,
        line_width=3.0,
    )

    c_theta = np.linspace(0, 2 * np.pi, 601)
    c_x = 8 * np.cos(c_theta)
    c_y = 8 * np.sin(c_theta)
    c_z = 5 * np.full(len(c_y), 1.0)

    position = np.array([c_x, c_y, c_z]).transpose()
    camera_data = {
        "position": position,
        "focal_point": np.array([(0.0, 0.0, 0.5 * np.pi)] * len(c_x)),
        "view_up": np.array([(0.0, 0.0, 1.0)] * len(c_x)),
    }

    parameter_0 = np.cos(np.linspace(0, 2 * np.pi, len(c_x)))
    parameter_1 = np.cos(np.linspace(0, 2 * np.pi, len(c_x)) + np.full(len(c_x), + 2 * np.pi / 3))
    parameter_2 = np.cos(np.linspace(0, 2 * np.pi, len(c_x)) + np.full(len(c_x), + 4 * np.pi / 3))

    points_0 = []
    points_1 = []
    points_2 = []

    points_data_0 = []
    points_data_1 = []
    points_data_2 = []


    for p0 in parameter_0:
        new_x = p0 * x0
        new_y = y0
        new_z = abs(p0) * z
        points_0.append(np.array([new_x, new_y, new_z]).transpose())
        points_data_0.append(np.full(len(z), np.pi * p0))

    for p1 in parameter_1:
        new_x = x1
        new_y = p1 * y1
        new_z = abs(p1) * z
        points_1.append(np.array([new_x, new_y, new_z]).transpose())
        points_data_1.append(np.full(len(z), np.pi * p1))

    for p2 in parameter_2:
        new_x = p2 * x1
        new_y = p2 * y1
        new_z = abs(p2) * z
        points_2.append(np.array([new_x, new_y, new_z]).transpose())
        points_data_2.append(np.full(len(z), np.pi * p2))

    actors_data = [
        {
            "position": [],
            "orientation": [],
            "points": points_0,
            "points_data": points_data_0,
        },
        {
            "position":[],
            "orientation":[],
            "points": points_1,
            "points_data": points_data_1,
        },
        {
            "position": [],
            "orientation": [],
            "points": points_2,
            "points_data": points_data_2,
        },
    ]

    renderer, interactor, window = t.create_animation(
        actors=actors,
        actors_data=actors_data,
        actors_2d=[],
        actors_2d_data=[],
        followers=[],
        followers_data=[],
        camera_data=camera_data,
        window_size=[1000, 1000],
        window_title="Plot",
        window_background=[0.0, 0.0, 0.0],
        refresh_rate=60,
        start_frame=0,
        n_frames=len(c_x),
        loop=False,
        save_frames_folder=None,
    )

    interactor.Start()


def test_modify_points():

    n_points = 1000

    theta = np.linspace(0, 10 * np.pi, n_points)
    z = np.linspace(0, np.pi, n_points)
    x0 = np.cos(theta) * np.sin(z)
    y0 = np.sin(theta) * np.sin(z)

    x1 = np.cos(theta + 2 * np.pi / 3) * np.sin(z)
    y1 = np.sin(theta + 2 * np.pi / 3) * np.sin(z)

    x2 = np.cos(theta + 4 * np.pi / 3) * np.sin(z)
    y2 = np.sin(theta + 4 * np.pi / 3) * np.sin(z)

    renderer, interactor, window, actors = t.plot_3d_lines(
        x_vectors=[x0, x1, x2],
        y_vectors=[y0, y1, y2],
        z_vectors=[z, z, z],
        data_vectors=[z, z, z],
        color_map="coolwarm",
        use_data_for_color=True,
        line_width=3.0,
    )

    new_points = np.array([1.5 * x0, 0.9 * y1, 1.1 * z]).transpose()
    new_points_data = np.full(len(z), 1000)

    t.modify_actor_points(actors[0], new_points, new_points_data)

    interactor.Start()


if __name__ == "__main__":

    # test_plot_3d_lines()
    # test_plot_3d_points()
    # test_plot_3d_lines_color_data()
    # test_plot_3d_points_color_data()
    # test_plot_3d_surfaces()
    # test_plot_3d_surfaces_color_data()
    # test_text_2d_plot()
    # test_text_3d_plot()
    # test_create_animation()
    # test_modify_points()
    test_create_animation_modify_points()
