import vtk
import vtk.util.colors as colors


def add_source(source, renderer, color=colors.tomato, flat_shading=True):

    source_mapper = vtk.vtkPolyDataMapper()
    source_mapper.SetInputConnection(source.GetOutputPort())
    source_actor = vtk.vtkActor()
    source_actor.SetMapper(source_mapper)

    source_actor.GetProperty().SetColor(color)

    if flat_shading:
        source_actor.GetProperty().SetInterpolationToFlat()

    renderer.AddActor(source_actor)

    return source_actor


def add_axis(renderer, scale):

    # Arrow
    arrow = vtk.vtkArrowSource()
    arrow.SetTipLength(0.05)
    arrow.SetTipRadius(0.1)
    arrow.SetTipResolution(25)
    arrow.SetShaftRadius(0.03)
    arrow.SetShaftResolution(25)
    arrow.SetInvert(False)

    x_axis = add_source(arrow, renderer, colors.red, flat_shading=True)
    y_axis = add_source(arrow, renderer, colors.green, flat_shading=True)
    z_axis = add_source(arrow, renderer, colors.blue, flat_shading=True)
    x_axis.SetScale([scale, .5, .5])
    y_axis.SetScale([scale, .5, .5])
    y_axis.RotateZ(90)
    z_axis.SetScale([scale, .5, .5])
    z_axis.RotateY(-90)


renderer = vtk.vtkRenderer()

add_axis(renderer, 10)

window = vtk.vtkRenderWindow()
window.AddRenderer(renderer)

interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(window)
interactor_style = vtk.vtkInteractorStyleTrackballCamera()
interactor.SetInteractorStyle(interactor_style)

renderer.SetBackground(1, 1, 1)
window.SetSize(800, 800)

interactor.Initialize()

renderer.ResetCamera()
renderer.GetActiveCamera().Zoom(1)
window.Render()

interactor.Start()
