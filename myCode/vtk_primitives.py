import vtk
import numpy as np
import vtk.util.colors as colors


def add_source(source, renderer, color=colors.tomato, flat_shading=True):

    source_mapper = vtk.vtkPolyDataMapper()
    source_mapper.SetInputConnection(source.GetOutputPort())
    source_actor = vtk.vtkActor()
    source_actor.SetMapper(source_mapper)

    source_actor.GetProperty().SetColor(color)

    if flat_shading:
        source_actor.GetProperty().SetInterpolationToFlat()

    renderer.AddActor(source_actor)

    return source_actor


# Cylinder Source
cylinder = vtk.vtkCylinderSource()
cylinder.SetResolution(100)

# Arc
arc = vtk.vtkArcSource()
arc.SetUseNormalAndAngle(True)
arc.SetNormal(np.array([0.0, 0.0, 1.0]))
arc.SetPolarVector(np.array([1.0, 0.0, 0.0]))
arc.SetAngle(300)
arc.SetResolution(100)

# Arrow
arrow = vtk.vtkArrowSource()
arrow.SetTipLength(0.05)
arrow.SetTipRadius(0.1)
arrow.SetTipResolution(25)
arrow.SetShaftRadius(0.03)
arrow.SetShaftResolution(25)
arrow.SetInvert(False)

# Bounded Points
bounded_points = vtk.vtkBoundedPointSource()
bounded_points.SetNumberOfPoints(100)
bounded_points.SetBounds(-5, 5, -5, 5, -5, 5)
bounded_points.SetProduceRandomScalars(False)
bounded_points.SetScalarRange(0, 10)
bounded_points.SetProduceCellOutput(True)

# Button
# button = vtk.vtkButtonSource()
# button.SetCenter(1, 1 ,1)
# button.SetTextureDimensions(5, 5)
# button.SetShoulderTextureCoordinate(0, 0)
# button.SetTwoSided(False)

# Capsule
capsule = vtk.vtkCapsuleSource()
capsule.SetRadius(1)
capsule.SetCenter(0, 0, 0)
capsule.SetCylinderLength(2)
capsule.SetThetaResolution(20)
capsule.SetPhiResolution(25)
capsule.SetLatLongTessellation(True)

# Cone
cone = vtk.vtkConeSource()
cone.SetHeight(10)
cone.SetRadius(3)
cone.SetResolution(25)
cone.SetCenter([0, 0, 0])
cone.SetDirection([0, 0, 1])

# Cube
cube = vtk.vtkCubeSource()
cube.SetXLength(10)
cube.SetYLength(10)
cube.SetZLength(10)
cube.SetCenter(0, 0, 0)
# or
# cube.SetBounds(-5, 10, -5, 12, -5, 4)

# Cylinder
cylinder = vtk.vtkCylinderSource()
cylinder.SetHeight(10)
cylinder.SetRadius(3)
cylinder.SetCenter([0, 0, 0])
cylinder.SetResolution(15)
cylinder.SetCapping(False)

# Disk
disk = vtk.vtkDiskSource()
disk.SetInnerRadius(5)
disk.SetOuterRadius(7)
disk.SetRadialResolution(5)
disk.SetCircumferentialResolution(50)

# Earth
earth = vtk.vtkEarthSource()
earth.SetRadius(10)
earth.SetOnRatio(1)
earth.SetOutline(True)

# Ellipse Arc Source
ellipse_arc = vtk.vtkEllipseArcSource()
ellipse_arc.SetCenter((0, 0, 0))
ellipse_arc.SetNormal((0, 0, 1))
ellipse_arc.SetMajorRadiusVector((1, 0, 0))
ellipse_arc.SetStartAngle(0)
ellipse_arc.SetSegmentAngle(120)
ellipse_arc.SetResolution(500)
ellipse_arc.SetRatio(0.5)

# Frustum
camera = vtk.vtkCamera()
planesArray = [0 for i in range(24)]
camera.GetFrustumPlanes(1, planesArray)
planes = vtk.vtkPlanes()
planes.SetFrustumPlanes(planesArray)

frustum = vtk.vtkFrustumSource()
frustum.SetPlanes(planes)
frustum.Update()

# Line
line = vtk.vtkLineSource()
line.SetPoint1(0, 0, 0)
line.SetPoint2(0, 1, 0)
line.SetUseRegularRefinement(True)
line.SetResolution(2)

# Outline
outline = vtk.vtkOutlineSource()
outline.SetBounds(-5, 5, -5, 5, -5, 5)
# or
# outline.SetBoxType(1)
# outline.SetCorners(
#    [
#        {0, 0, 0},
#        {1, 0, 0},
#        {0, 1, 0},
#        {1, 1, 0},
#        {0, 0, 1},
#        {1, 0, 1},
#        {0, 1, 1},
#        {1, 1, 1},
#    ]
# )

# Plane
plane = vtk.vtkPlaneSource()
plane.SetXResolution(10)
plane.SetYResolution(10)
plane.SetResolution(20, 20)
plane.SetOrigin(0, 0, 0)
plane.SetPoint1(10, 0, 0)
plane.SetPoint2(0, 10, 0)
#plane.SetCenter(0, 0, 0)
#plane.SetNormal(0, 0, 1)
#plane.Push(0)

# Platonic Solid
platonic_solid = vtk.vtkPlatonicSolidSource()
platonic_solid.SetSolidType(4)

# Points
points = vtk.vtkPointSource()
points.SetNumberOfPoints(1000)
points.SetCenter([0, 0, 1])
points.SetRadius(5)
points.SetDistribution(1)

# Regular Polygon
regular_polygon = vtk.vtkRegularPolygonSource()
regular_polygon.SetNumberOfSides(6)
regular_polygon.SetCenter([0, 0, 0])
regular_polygon.SetNormal([1, 0, 0])
regular_polygon.SetRadius(5)

# Disk Sector
disk_sector = vtk.vtkSectorSource()
disk_sector.SetInnerRadius(5)
disk_sector.SetOuterRadius(7)
disk_sector.SetZCoord(3)
disk_sector.SetRadialResolution(3)
disk_sector.SetCircumferentialResolution(15)
disk_sector.SetStartAngle(0)
disk_sector.SetEndAngle(30)

# Sphere
sphere = vtk.vtkSphereSource()
sphere.SetRadius(5)
sphere.SetCenter((0, 2, -6))
sphere.SetThetaResolution(100)
sphere.SetPhiResolution(100)
sphere.SetStartTheta(0)
sphere.SetEndTheta(90)
sphere.SetStartPhi(-90)
sphere.SetEndPhi(90)
sphere.SetLatLongTessellation(False)

# Superquadric
superquadric = vtk.vtkSuperquadricSource()
superquadric.SetCenter(0, 0, 0)
superquadric.SetScale(1, 2, 3)
superquadric.SetThetaResolution(30)
superquadric.SetPhiResolution(30)
superquadric.SetThickness(0.3)
superquadric.SetPhiRoundness(0.25)
superquadric.SetThetaRoundness(0.1)
superquadric.SetAxisOfSymmetry(0)
superquadric.SetSize(1)
superquadric.SetToroidal(True)

# Box
box = vtk.vtkTessellatedBoxSource()
box.SetBounds([-5, 5, -5, 5, -5, 5])
box.SetLevel(10)
box.SetQuads(False)

# Text
text = vtk.vtkTextSource()
text.SetText("Hello!")
text.SetBacking(True)
text.SetForegroundColor(colors.red)
text.SetBackgroundColor(colors.blue)

# Textured Sphere
textured_sphere = vtk.vtkTexturedSphereSource()
textured_sphere.SetRadius(5)
textured_sphere.SetThetaResolution(25)
textured_sphere.SetPhiResolution(25)
textured_sphere.SetTheta(45)
textured_sphere.SetPhi(10)


renderer = vtk.vtkRenderer()

window = vtk.vtkRenderWindow()
window.AddRenderer(renderer)

source_actor1 = add_source(arrow, renderer, colors.red, flat_shading=True)
source_actor2 = add_source(arrow, renderer, colors.green, flat_shading=True)
source_actor3 = add_source(arrow, renderer, colors.blue, flat_shading=True)
source_actor1.SetScale([10, .5, .5])
source_actor2.SetScale([10, .5, .5])
source_actor2.RotateZ(90)
source_actor3.SetScale([10, .5, .5])
source_actor3.RotateY(-90)

source_actor4 = add_source(plane, renderer, colors.grey, flat_shading=True)

interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(window)
interactor_style = vtk.vtkInteractorStyleTrackballCamera()
interactor.SetInteractorStyle(interactor_style)

renderer.SetBackground(1, 1, 1)
window.SetSize(500, 500)

interactor.Initialize()

renderer.ResetCamera()
renderer.GetActiveCamera().Zoom(1)
window.Render()

interactor.Start()
