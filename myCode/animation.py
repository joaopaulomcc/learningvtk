import vtk

# import time
import vtk.util.colors as colors

import numpy as np


def add_source(source, renderer, color=colors.tomato, flat_shading=True):

    source_mapper = vtk.vtkPolyDataMapper()
    source_mapper.SetInputConnection(source.GetOutputPort())
    source_actor = vtk.vtkActor()
    source_actor.SetMapper(source_mapper)

    source_actor.GetProperty().SetColor(color)

    if flat_shading:
        source_actor.GetProperty().SetInterpolationToFlat()

    renderer.AddActor(source_actor)

    return source_actor


def add_axis(renderer, scale):

    # Arrow
    arrow = vtk.vtkArrowSource()
    arrow.SetTipLength(0.05)
    arrow.SetTipRadius(0.1)
    arrow.SetTipResolution(25)
    arrow.SetShaftRadius(0.03)
    arrow.SetShaftResolution(25)
    arrow.SetInvert(False)

    x_axis = add_source(arrow, renderer, colors.red, flat_shading=True)
    y_axis = add_source(arrow, renderer, colors.green, flat_shading=True)
    z_axis = add_source(arrow, renderer, colors.blue, flat_shading=True)
    x_axis.SetScale([scale, 0.5, 0.5])
    y_axis.SetScale([scale, 0.5, 0.5])
    y_axis.RotateZ(90)
    z_axis.SetScale([scale, 0.5, 0.5])
    z_axis.RotateY(-90)


def initial_velocity_vector(elevation, azimuth, velocity):

    v_z = velocity * np.sin(np.radians(elevation))
    v_horizontal = velocity * np.cos(np.radians(elevation))
    v_x = v_horizontal * np.cos(np.radians(azimuth))
    v_y = v_horizontal * np.sin(np.radians(azimuth))

    return np.array([v_x, v_y, v_z])


def update_velocity(velocity_vector, delta_t, gravity):

    v_x = velocity_vector[0]
    v_y = velocity_vector[1]
    v_z = velocity_vector[2] + gravity * delta_t

    return np.array([v_x, v_y, v_z])


def update_positon(position_vector, velocity_vector, delta_t):

    return position_vector + velocity_vector * delta_t


def calculate_tragectory(
    elevation,
    azimuth,
    initial_velocity,
    initial_position,
    gravity,
    simulation_time,
    delta_t,
):

    velocity_vector = initial_velocity_vector(elevation, azimuth, initial_velocity)
    position_vector = initial_position

    time_vector = np.arange(0, simulation_time, delta_t)
    x_vector = np.zeros(len(time_vector))
    y_vector = np.zeros(len(time_vector))
    z_vector = np.zeros(len(time_vector))
    v_x_vector = np.zeros(len(time_vector))
    v_y_vector = np.zeros(len(time_vector))
    v_z_vector = np.zeros(len(time_vector))

    for i, _ in enumerate(time_vector):

        if i == 0:
            x_vector[i] = position_vector[0]
            y_vector[i] = position_vector[1]
            z_vector[i] = position_vector[2]
            v_x_vector[i] = velocity_vector[0]
            v_y_vector[i] = velocity_vector[1]
            v_z_vector[i] = velocity_vector[2]

        else:
            position_vector = update_positon(position_vector, velocity_vector, delta_t)
            velocity_vector = update_velocity(velocity_vector, delta_t, gravity)

            if position_vector[2] < 0:
                velocity_vector[2] = -0.8 * velocity_vector[2]
                position_vector[2] = 0

            x_vector[i] = position_vector[0]
            y_vector[i] = position_vector[1]
            z_vector[i] = position_vector[2]
            v_x_vector[i] = velocity_vector[0]
            v_y_vector[i] = velocity_vector[1]
            v_z_vector[i] = velocity_vector[2]

    positions = np.array([x_vector, y_vector, z_vector]).transpose()
    velocities = np.array([v_x_vector, v_y_vector, v_z_vector]).transpose()

    # print(positions)

    return (
        time_vector,
        positions,
        velocities,
    )


def main():

    gravity = -9.81
    elevation = 75
    azimuth = 30
    initial_velocity = 15
    initial_position = np.array([0, 0, 0])
    simulation_time = 7
    delta_t = simulation_time / 1000
    refresh_rate = 60

    time_vector, positions, velocities = calculate_tragectory(
        elevation,
        azimuth,
        initial_velocity,
        initial_position,
        gravity,
        simulation_time,
        delta_t,
    )

    sphere = vtk.vtkSphereSource()
    sphere.SetRadius(0.3)
    sphere.SetCenter(initial_position)

    plane = vtk.vtkPlaneSource()
    plane.SetXResolution(10)
    plane.SetYResolution(10)
    plane.SetResolution(20, 20)
    plane.SetOrigin(0, 0, 0)
    plane.SetPoint1(20, 0, 0)
    plane.SetPoint2(0, 20, 0)

    renderer = vtk.vtkRenderer()

    add_axis(renderer, 15)
    sphere_actor = add_source(sphere, renderer, colors.red, flat_shading=True)
    _ = add_source(plane, renderer, colors.grey, flat_shading=True)

    window = vtk.vtkRenderWindow()
    window.AddRenderer(renderer)
    window.SetWindowName("Test")

    interactor = vtk.vtkRenderWindowInteractor()
    interactor.SetRenderWindow(window)
    interactor_style = vtk.vtkInteractorStyleTrackballCamera()
    interactor.SetInteractorStyle(interactor_style)
    interactor.Initialize()
    interactor.CreateRepeatingTimer(int(1 / refresh_rate))

    writer = vtk.vtkPNGWriter()
    w2if = vtk.vtkWindowToImageFilter()
    w2if.SetInput(window)
    w2if.SetInputBufferTypeToRGB()
    w2if.ReadFrontBufferOff()
    w2if.Update()

    i = 0
    callback_func.i = i
    callback_func.positions = positions
    callback_func.sphere_actor = sphere_actor
    callback_func.renderer = renderer
    callback_func.window = window
    callback_func.writer = writer
    callback_func.w2if = w2if

    interactor.AddObserver("TimerEvent", callback_func)

    renderer.SetBackground(1, 1, 1)
    window.SetSize(800, 800)

    # renderer.GetActiveCamera().Zoom(1)
    renderer.GetActiveCamera().SetViewUp((0, 0, 1))
    renderer.GetActiveCamera().SetFocalPoint((5, 5, 5))
    renderer.GetActiveCamera().SetFocalPoint((5, 5, 0))
    renderer.GetActiveCamera().SetPosition((10, 0, 5))
    renderer.ResetCamera()

    window.Render()

    # for position in positions:
    #    sphere_actor.SetPosition(position)
    #    renderer.ResetCameraClippingRange()
    #    # renderer.ResetCamera()
    #    window.Render()
    #    time.sleep(10 / len(positions))

    interactor.Start()


def callback_func(caller, timer_event):
    # We rotate the arrow by 1 degree in the Z direction.
    # This creates a nice counterclockwise motion
    # for the default camera postion/direction.
    # It is possible to speed/slow the motion by changing
    # this value, or the refresh rate.

    if callback_func.i < len(callback_func.positions):
        callback_func.position = callback_func.positions[callback_func.i]

    else:
        callback_func.i = 0

        callback_func.position = callback_func.positions[callback_func.i]

    callback_func.sphere_actor.SetPosition(callback_func.position)
    callback_func.renderer.ResetCameraClippingRange()
    # renderer.ResetCamera()
    callback_func.window.Render()

    # callback_func.w2if.Modified()
    # callback_func.w2if.Update()
    # callback_func.writer.SetFileName(f"myCode/output/screenshot{callback_func.i}.png")
    # callback_func.writer.SetInputData(callback_func.w2if.GetOutput())
    # callback_func.writer.Write()

    callback_func.i += 1

    # time.sleep(10 / len(positions))


if __name__ == "__main__":

    main()