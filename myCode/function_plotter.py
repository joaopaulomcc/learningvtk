import vtk
from matplotlib import cm

import numpy as np

resolution = 100

x = np.linspace(-2 * np.pi, 2 * np.pi, resolution)
y = np.linspace(-2 * np.pi, 2 * np.pi, resolution)
xx, yy = np.meshgrid(x, y)

zz = 2 * np.sin(0.2 * (xx ** 2 + yy ** 2))
# zz = 0.75 / np.exp((xx * 5) ** 2 * (yy * 5) ** 2)
# zz = np.cos(xx ** 2 + yy ** 2 - 0.5) - 0.5
# zz = (0.4 ** 2 - (0.6 - (xx ** 2 + yy ** 2) ** 0.5) ** 2) ** 0.5
# zz = 1 / (15 * (xx ** 2 + yy ** 2))

x_data = xx.flatten()
y_data = yy.flatten()
z_data = zz.flatten()

points = vtk.vtkPoints()
cells = vtk.vtkCellArray()
mesh = vtk.vtkPolyData()
tri = vtk.vtkTriangle()

for x_value, y_value, z_value in zip(x_data, y_data, z_data):

    points.InsertNextPoint(x_value, y_value, z_value)

id = 0

for i in range(resolution - 1):
    for j in range(resolution - 1):
        quad = vtk.vtkQuad()
        quad.GetPointIds().SetId(0, id)
        quad.GetPointIds().SetId(1, id + resolution)
        quad.GetPointIds().SetId(2, id + resolution + 1)
        quad.GetPointIds().SetId(3, id + 1)
        cells.InsertNextCell(quad)

        id += 1
    id += 1

points_data = vtk.vtkDoubleArray()
points_data.SetNumberOfComponents(1)

for z_value in z_data:
    points_data.InsertNextTuple([z_value])

mesh.SetPoints(points)
mesh.SetPolys(cells)
mesh.GetPointData().SetScalars(points_data)

n_color_lut = 1000
n_color_interpolation = 100
color_map = "viridis"

cmap = cm.get_cmap(color_map)

lut = vtk.vtkLookupTable()
lut.SetTableRange(np.min(points_data), np.max(points_data))
lut.SetNumberOfColors(n_color_lut)

ctransfer = vtk.vtkColorTransferFunction()

for value in np.linspace(0, 1, n_color_interpolation):
    rgba = cmap(value)
    ctransfer.AddRGBPoint(value, rgba[0], rgba[1], rgba[2])

for i, value in enumerate(np.linspace(0, 1, n_color_lut)):
    color = ctransfer.GetColor((value))
    lut.SetTableValue(i, *color)

lut.Build()

color_bar_actor = vtk.vtkScalarBarActor()
#color_bar_actor.SetNumberOfLabels(0)
color_bar_actor.SetLookupTable(lut)
color_bar_actor.GetLabelTextProperty().SetColor(0, 0, 0)

norms_generator = vtk.vtkPolyDataNormals()
norms_generator.SetInputData(mesh)


mesh_mapper = vtk.vtkPolyDataMapper()
mesh_mapper.SetInputData(mesh)
mesh_mapper.SetLookupTable(lut)
mesh_mapper.SetUseLookupTableScalarRange(True)
mesh_actor = vtk.vtkActor()
mesh_actor.SetMapper(mesh_mapper)

renderer = vtk.vtkRenderer()
renderer.SetBackground(1, 1, 1)

axes = vtk.vtkCubeAxesActor2D()
axes.SetInputConnection(norms_generator.GetOutputPort())
axes.SetCamera(renderer.GetActiveCamera())
axes.SetLabelFormat("%1.1g")
axes.GetAxisLabelTextProperty().SetColor(0, 0, 0)
axes.GetAxisTitleTextProperty().SetColor(0, 0, 0)
axes.GetProperty().SetColor(0, 0, 0)


outline_filter = vtk.vtkOutlineFilter()
outline_filter.SetInputConnection(norms_generator.GetOutputPort())
outline_mapper = vtk.vtkPolyDataMapper()
outline_mapper.SetInputConnection(outline_filter.GetOutputPort())
outline_actor = vtk.vtkActor()
outline_actor.SetMapper(outline_mapper)
outline_actor.GetProperty().SetColor(0, 0, 0)


renderer.AddActor(mesh_actor)
renderer.AddActor(axes)
#renderer.AddActor(outline_actor)
renderer.AddActor2D(color_bar_actor)

window = vtk.vtkRenderWindow()
window.SetSize(1000, 800)
window.AddRenderer(renderer)

interactor = vtk.vtkRenderWindowInteractor()
interactor.SetRenderWindow(window)
interactor_style = vtk.vtkInteractorStyleTrackballCamera()
interactor.SetInteractorStyle(interactor_style)

interactor.Initialize()

renderer.ResetCamera()

window.Render()
window.SetWindowName("Function Plotter")

interactor.Start()
