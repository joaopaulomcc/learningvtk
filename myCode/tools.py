import numpy as np

import vtk

from matplotlib import cm
from pathlib import Path


def create_vtk_scene(
    actors=[],
    actors_2d=[],
    followers=[],
    window_size=[1000, 1000],
    window_title="Plot",
    window_background=[0.0, 0.0, 0.0],
):
    """Creates a VTK scene and add the provided actors"""

    window_size = [i for i in map(int, window_size)]
    window_background = [f for f in map(float, window_background)]

    renderer = vtk.vtkRenderer()
    renderer.SetBackground(window_background)
    renderer.UseFXAAOn()

    for actor in actors:
        renderer.AddActor(actor)

    for actor_2d in actors_2d:
        renderer.AddActor2D(actor_2d)

    for follower in followers:
        renderer.AddActor(follower)
        follower.SetCamera(renderer.GetActiveCamera())

    window = vtk.vtkRenderWindow()
    window.SetSize(window_size)
    window.AddRenderer(renderer)

    window.PointSmoothingOn()
    window.LineSmoothingOn()
    window.Render()
    window.SetWindowName(window_title)

    interactor = vtk.vtkRenderWindowInteractor()
    interactor.SetRenderWindow(window)
    interactor_style = vtk.vtkInteractorStyleTrackballCamera()
    interactor.SetInteractorStyle(interactor_style)

    renderer.GetActiveCamera().SetViewUp((0, 0, 1))
    renderer.GetActiveCamera().SetFocalPoint((0, 0, 0))
    renderer.GetActiveCamera().SetPosition((1, 1, 1))
    renderer.ResetCamera()

    interactor.Initialize()

    return renderer, interactor, window


def create_animation(
    actors=[],
    actors_data=[],
    actors_2d=[],
    actors_2d_data=[],
    followers=[],
    followers_data=[],
    camera_data=[],
    window_size=[1000, 1000],
    window_title="Plot",
    window_background=[0.0, 0.0, 0.0],
    refresh_rate=60,
    start_frame=0,
    n_frames=None,
    loop=False,
    save_frames_folder=None,
):

    renderer, interactor, window = create_vtk_scene(
        actors=actors,
        actors_2d=actors_2d,
        followers=followers,
        window_size=window_size,
        window_title=window_title,
        window_background=window_background,
    )

    interactor.CreateRepeatingTimer(int(1 / refresh_rate))

    frame = start_frame
    update_animation.frame = frame
    update_animation.start_frame = start_frame
    update_animation.n_frames = n_frames
    update_animation.loop = loop
    update_animation.save_frames_folder = save_frames_folder

    update_animation.actors = actors
    update_animation.actors_data = actors_data
    update_animation.actors_2d = actors_2d
    update_animation.actors_2d_data = actors_2d_data
    update_animation.followers = followers
    update_animation.followers_data = followers_data
    update_animation.camera_data = camera_data

    update_animation.renderer = renderer
    update_animation.interactor = interactor
    update_animation.window = window

    interactor.AddObserver("TimerEvent", update_animation)

    return renderer, interactor, window


def update_animation(caller, timer_event):

    for actor, actor_data in zip(update_animation.actors, update_animation.actors_data):

        if len(actor_data["position"]) > 0:
            actor_position = actor_data["position"][update_animation.frame]
            actor.SetPosition(actor_position)
        
        if len(actor_data["orientation"]) > 0:
            actor_orientation = actor_data["orientation"][update_animation.frame]        
            actor.SetOrientation(actor_orientation)

        if len(actor_data["points"]) > 0:
            new_points = actor_data["points"][update_animation.frame]
            modify_actor_points(actor, new_points=new_points, new_data_vector=None)

        if len(actor_data["points_data"]) > 0:
            new_points_data = actor_data["points_data"][update_animation.frame]
            modify_actor_points(actor, new_points=None, new_data_vector=new_points_data)

    for actor_2d, actor_2d_data in zip(update_animation.actors_2d, update_animation.actors_2d_data):

        if len(actor_2d_data["position"]) > 0:

            actor_position = actor_2d_data["position"][update_animation.frame]
            actor_2d.SetPosition(actor_position)

        if len(actor_2d_data["orientation"]) > 0:

            actor_orientation = actor_2d_data["orientation"][update_animation.frame]
            actor_2d.SetOrientation(actor_orientation)

    for follower, follower_data in zip(update_animation.followers, update_animation.followers_data):

        if len(follower_data["position"]) > 0:

            follower_position = follower_data["position"][update_animation.frame]
            follower.SetPosition(follower_position)

    if update_animation.camera_data:

        if len(update_animation.camera_data["position"]) > 0:
            camera_position = update_animation.camera_data["position"][update_animation.frame]
            update_animation.renderer.GetActiveCamera().SetPosition(camera_position)

        if len(update_animation.camera_data["focal_point"]) > 0:
            camera_focal_point = update_animation.camera_data["focal_point"][update_animation.frame]
            update_animation.renderer.GetActiveCamera().SetFocalPoint(camera_focal_point)

        if len(update_animation.camera_data["view_up"]) > 0:
            camera_view_up = update_animation.camera_data["view_up"][update_animation.frame]        
            update_animation.renderer.GetActiveCamera().SetViewUp(camera_view_up)

    update_animation.renderer.ResetCameraClippingRange()
    update_animation.window.Render()

    if update_animation.save_frames_folder:
        frame_path = Path(update_animation.save_frames_folder) / Path(
            f"frame_{update_animation.frame}.png"
        )
        save_picture(update_animation.window, frame_path)


    if update_animation.frame == update_animation.n_frames - 1:
        update_animation.frame = update_animation.start_frame
    else:
        update_animation.frame += 1


def create_vtk_line(x_vector, y_vector, z_vector, data_vector=None):

    points = vtk.vtkPoints()
    lines = vtk.vtkCellArray()
    mesh = vtk.vtkPolyData()

    for x, y, z in zip(x_vector, y_vector, z_vector):

        points.InsertNextPoint(x, y, z)

    for i in range(len(x_vector) - 1):

        line = vtk.vtkLine()
        line.GetPointIds().SetId(0, i)
        line.GetPointIds().SetId(1, i + 1)
        lines.InsertNextCell(line)

    if data_vector is not None:

        points_data = vtk.vtkDoubleArray()
        points_data.SetNumberOfComponents(1)

        for value in data_vector:
            points_data.InsertNextTuple([value])

    mesh.SetPoints(points)
    mesh.SetLines(lines)

    if data_vector is not None:
        mesh.GetPointData().SetScalars(points_data)

    return mesh


def create_vtk_points(x_vector, y_vector, z_vector, data_vector=None):

    points = vtk.vtkPoints()
    vertices = vtk.vtkCellArray()
    mesh = vtk.vtkPolyData()

    for x, y, z in zip(x_vector, y_vector, z_vector):

        points.InsertNextPoint(x, y, z)

    for i in range(len(x_vector)):
        vertex = vtk.vtkVertex()
        vertex.GetPointIds().SetId(0, i)
        vertices.InsertNextCell(vertex)

    if data_vector is not None:

        points_data = vtk.vtkDoubleArray()
        points_data.SetNumberOfComponents(1)

        for value in data_vector:
            points_data.InsertNextTuple([value])

    mesh.SetPoints(points)
    mesh.SetVerts(vertices)

    if data_vector is not None:
        mesh.GetPointData().SetScalars(points_data)

    return mesh


def create_vtk_surface(xx_matrix, yy_matrix, zz_matrix, data_matrix=None):

    x_vector = xx_matrix.flatten()
    y_vector = yy_matrix.flatten()
    z_vector = zz_matrix.flatten()

    points_shape = np.shape(xx_matrix)

    points = vtk.vtkPoints()
    cells = vtk.vtkCellArray()
    mesh = vtk.vtkPolyData()
    tri = vtk.vtkTriangle()

    for x_value, y_value, z_value in zip(x_vector, y_vector, z_vector):

        points.InsertNextPoint(x_value, y_value, z_value)

    id = 0

    for i in range(points_shape[0] - 1):
        for j in range(points_shape[1] - 1):
            quad = vtk.vtkQuad()
            quad.GetPointIds().SetId(0, id)
            quad.GetPointIds().SetId(1, id + points_shape[1])
            quad.GetPointIds().SetId(2, id + points_shape[1] + 1)
            quad.GetPointIds().SetId(3, id + 1)
            cells.InsertNextCell(quad)

            id += 1
        id += 1

    mesh.SetPoints(points)
    mesh.SetPolys(cells)

    if data_matrix is not None:
        data_vector = data_matrix.flatten()
        data_shape = np.shape(data_matrix)
        data = vtk.vtkDoubleArray()
        data.SetNumberOfComponents(1)

        for value in data_vector:
            data.InsertNextTuple([value])

        if data_shape == points_shape:
            mesh.GetPointData().SetScalars(data)
        else:
            mesh.GetCellData().SetScalars(data)

    return mesh


def create_vtk_2d_text(
    text_string,
    position,
    font_size=12,
    text_color=[1.0, 1.0, 1.0],
    font_family="arial",
    bold=False,
    italic=False,
    shadow=False,
    line_spacing=0.8,
    justification="left",
    vertical_justification="bottom",
):

    font_family_dict = {
        "arial": 0,
        "courier": 1,
        "times": 2,
    }

    justification_dict = {
        "left": 0,
        "center": 1,
        "right": 2,
    }

    vertical_justification_dict = {
        "bottom": 0,
        "middle": 1,
        "top": 2,
    }

    text_prop = vtk.vtkTextProperty()
    text_prop.SetFontSize(font_size)
    text_prop.SetFontFamily(font_family_dict[font_family])
    text_prop.SetLineSpacing(line_spacing)
    text_prop.SetBold(bold)
    text_prop.SetItalic(italic)
    text_prop.SetShadow(shadow)
    text_prop.SetColor(text_color)
    text_prop.SetJustification(justification_dict[justification])
    text_prop.SetVerticalJustification(vertical_justification_dict[vertical_justification])

    text_mapper = vtk.vtkTextMapper()
    text_mapper.SetInput(text_string)
    text_mapper.SetTextProperty(text_prop)

    text_actor = vtk.vtkActor2D()
    text_actor.SetMapper(text_mapper)
    text_actor.GetPositionCoordinate().SetCoordinateSystemToNormalizedDisplay()
    x, y = position
    text_actor.GetPositionCoordinate().SetValue(x, y)

    return text_actor


def create_vtk_3d_text(
    text_string,
    position,
    font_size=12,
    text_color=[1.0, 1.0, 1.0],
):

    text_mesh = vtk.vtkVectorText()
    text_mesh.SetText(text_string)
    text_mapper = vtk.vtkPolyDataMapper()
    text_mapper.SetInputConnection(text_mesh.GetOutputPort())
    text_actor = vtk.vtkFollower()
    text_actor.SetMapper(text_mapper)
    text_actor.SetScale(font_size, font_size, font_size)
    x, y, z = position
    text_actor.AddPosition(x, y, z)
    text_actor.GetProperty().SetColor(text_color)

    return text_actor


def plot_3d_lines(
    x_vectors,
    y_vectors,
    z_vectors,
    data_vectors=None,
    color_map="viridis",
    use_data_for_color=False,
    colors=[1.0, 1.0, 1.0],
    line_width=1.0,
):

    colors_shape = np.shape(colors)

    if len(x_vectors[0]) == 1:

        x_vectors = [x_vectors]
        y_vectors = [y_vectors]
        z_vectors = [z_vectors]

    actors = []

    for i, (x_vector, y_vector, z_vector) in enumerate(zip(x_vectors, y_vectors, z_vectors)):

        if data_vectors is None:
            mesh = create_vtk_line(x_vector, y_vector, z_vector)
        else:
            mesh = create_vtk_line(x_vector, y_vector, z_vector, data_vectors[i])
        mesh_mapper = vtk.vtkPolyDataMapper()
        mesh_mapper.SetInputData(mesh)

        if use_data_for_color:

            lookup_table = generate_color_lookup_table(data_vectors[i], color_map=color_map)
            mesh_mapper.SetLookupTable(lookup_table)
            mesh_mapper.SetUseLookupTableScalarRange(True)

            mesh_actor = vtk.vtkActor()
            mesh_actor.SetMapper(mesh_mapper)

        else:

            mesh_actor = vtk.vtkActor()
            mesh_actor.SetMapper(mesh_mapper)

            if len(colors_shape) == 1:
                mesh_actor.GetProperty().SetColor(colors)

            else:
                mesh_actor.GetProperty().SetColor(colors[i])

        mesh_actor.GetProperty().SetLineWidth(line_width)

        actors.append(mesh_actor)

    renderer, interactor, window = create_vtk_scene(actors)

    return renderer, interactor, window, actors


def plot_3d_points(
    x_vectors,
    y_vectors,
    z_vectors,
    data_vectors=None,
    color_map="viridis",
    use_data_for_color=False,
    colors=[1.0, 1.0, 1.0],
    point_size=1.0,
):

    colors_shape = np.shape(colors)

    if len(x_vectors[0]) == 1:

        x_vectors = [x_vectors]
        y_vectors = [y_vectors]
        z_vectors = [z_vectors]
        data_vectors = [data_vectors]

    actors = []

    for i, (x_vector, y_vector, z_vector) in enumerate(zip(x_vectors, y_vectors, z_vectors)):

        if data_vectors is None:
            mesh = create_vtk_points(x_vector, y_vector, z_vector)
        else:
            mesh = create_vtk_points(x_vector, y_vector, z_vector, data_vectors[i])

        mesh_mapper = vtk.vtkPolyDataMapper()
        mesh_mapper.SetInputData(mesh)

        if use_data_for_color:

            lookup_table = generate_color_lookup_table(data_vectors[i], color_map=color_map)
            mesh_mapper.SetLookupTable(lookup_table)
            mesh_mapper.SetUseLookupTableScalarRange(True)

            mesh_actor = vtk.vtkActor()
            mesh_actor.SetMapper(mesh_mapper)

        else:

            mesh_actor = vtk.vtkActor()
            mesh_actor.SetMapper(mesh_mapper)

            if len(colors_shape) == 1:
                mesh_actor.GetProperty().SetColor(colors)

            else:
                mesh_actor.GetProperty().SetColor(colors[i])

        mesh_actor.GetProperty().SetPointSize(point_size)

        actors.append(mesh_actor)

    renderer, interactor, window = create_vtk_scene(actors)

    return renderer, interactor, window, actors


def plot_3d_surfaces(
    xx_matrices,
    yy_matrices,
    zz_matrices,
    data_matrices=None,
    color_map="viridis",
    use_data_for_color=False,
    colors=[1.0, 1.0, 1.0],
):

    colors_shape = np.shape(colors)

    if xx_matrices.ndim == 2:

        xx_matrices = [xx_matrices]
        yy_matrices = [yy_matrices]
        zz_matrices = [zz_matrices]
        data_matrices = [data_matrices]

    actors = []

    for i, (xx_matrix, yy_matrix, zz_matrix) in enumerate(
        zip(xx_matrices, yy_matrices, zz_matrices)
    ):

        if data_matrices is None:
            mesh = create_vtk_surface(xx_matrix, yy_matrix, zz_matrix)
        else:
            mesh = create_vtk_surface(xx_matrix, yy_matrix, zz_matrix, data_matrices[i])

        mesh_mapper = vtk.vtkPolyDataMapper()
        mesh_mapper.SetInputData(mesh)

        if use_data_for_color:

            lookup_table = generate_color_lookup_table(
                data_matrices[i].flatten(), color_map=color_map
            )
            mesh_mapper.SetLookupTable(lookup_table)
            mesh_mapper.SetUseLookupTableScalarRange(True)

            mesh_actor = vtk.vtkActor()
            mesh_actor.SetMapper(mesh_mapper)

        else:

            mesh_mapper.ScalarVisibilityOff()
            mesh_actor = vtk.vtkActor()
            mesh_actor.SetMapper(mesh_mapper)

            if len(colors_shape) == 1:
                mesh_actor.GetProperty().SetColor(colors)

            else:
                mesh_actor.GetProperty().SetColor(colors[i])

        actors.append(mesh_actor)

    renderer, interactor, window = create_vtk_scene(actors)

    return renderer, interactor, window, actors


def generate_color_lookup_table(
    points_data, n_colors=256, color_map="viridis", n_colormap_samples=100
):

    cmap = cm.get_cmap(color_map)

    lookup_table = vtk.vtkLookupTable()
    lookup_table.SetTableRange(np.min(points_data), np.max(points_data))
    lookup_table.SetNumberOfColors(n_colors)

    color_transfer = vtk.vtkColorTransferFunction()

    for value in np.linspace(0, 1, n_colormap_samples):
        rgba = cmap(value)
        color_transfer.AddRGBPoint(value, rgba[0], rgba[1], rgba[2])

    for i, value in enumerate(np.linspace(0, 1, n_colors)):
        color = color_transfer.GetColor((value))
        lookup_table.SetTableValue(i, *color)

    lookup_table.Build()

    return lookup_table


def save_picture(window, picture_path):

    writer = vtk.vtkPNGWriter()
    w2if = vtk.vtkWindowToImageFilter()
    w2if.SetInput(window)
    w2if.SetInputBufferTypeToRGB()
    w2if.ReadFrontBufferOff()
    w2if.Modified()
    w2if.Update()
    writer.SetFileName(picture_path)
    writer.SetInputData(w2if.GetOutput())
    writer.Write()


def modify_actor_points(actor, new_points=None, new_data_vector=None):

    points = actor.GetMapper().GetInput().GetPoints()

    if new_points is not None:

        for i in range(points.GetNumberOfPoints()):
            points.SetPoint(i, new_points[i])

    if new_data_vector is not None:

        new_points_data = vtk.vtkDoubleArray()
        new_points_data.SetNumberOfComponents(1)

        for value in new_data_vector:
            new_points_data.InsertNextTuple([value])

        actor.GetMapper().GetInput().GetPointData().SetScalars(new_points_data)

    points.Modified()

    return points

